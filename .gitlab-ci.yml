variables: &variables
  DEBIAN_FRONTEND: noninteractive
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ""
  # Build parameters. We later override them at job level if needed.
  RUN_TESTS: "ON"
  STOCK_CPPZMQ: "ON"
  STOCK_OMNIORB: "ON"
  CMAKE_DISABLE_PRECOMPILE_HEADERS: "OFF"
  CMAKE_BUILD_TYPE: "Debug"
  TANGO_USE_JPEG: "ON"
  BUILD_SHARED_LIBS: "ON"
  TANGO_USE_LIBCPP: "OFF"
  TOOLCHAIN_FILE: ""
  JPEG_LIB: libjpeg-dev
  TANGO_IDL_TAG: "5.1.2"
  CTEST_PARALLEL_LEVEL: "1"

services:
  - docker:20.10.16-dind

# See: https://docs.gitlab.com/ce/ci/yaml/README.html#workflowrules-templates
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.job-template: &job-template
  image: debian:10
  tags:
    - dind, skao, docker, linux, amd64
  before_script:
    - ulimit -c unlimited
    - echo "core.%e.%p.%t" > /proc/sys/kernel/core_pattern
    - apt-get update && apt-get install -y git wget unzip docker.io
    - ci/before_install.sh
    - docker build --build-arg JPEG_LIB=${JPEG_LIB} -t cpp_tango ci/${OS_TYPE}
    - >
        docker run
        --rm
        --name cpp_tango
        --user root
        --ulimit core=-1
        -e TANGO_HOST=${TANGO_HOST}
        -e TOOLCHAIN_FILE=${TOOLCHAIN_FILE}
        -e DOCKER_HOST=tcp://$(getent hosts docker | awk '{ print $1 }'):2375
        -v `pwd`:/home/tango/src
        -v `pwd`/idl:/home/tango/idl
        -v `pwd`/cppzmq:/home/tango/cppzmq
        -v `pwd`/omniorb:/home/tango/omniorb
        -v `pwd`/build-wrapper-linux-x86:/home/tango/build-wrapper-linux-x86
        -dit
        cpp_tango
    - ci/install_tango_idl.sh
    - if [ ${STOCK_CPPZMQ} = "OFF" ]; then ci/install_cppzmq.sh; fi
    - if [ ${STOCK_OMNIORB} = "OFF" ]; then ci/install_omniorb.sh; fi
  script:
    - ci/run.sh
    - ci/test.sh
  after_script:
    - ci/print_coredumps.sh
    - docker stop cpp_tango || true
    - mkdir -p build/tests/results
  artifacts:
    when: always
    paths:
      - build/tests/results
      - build/tests/core.*

abi-api-compliance-check:
  variables:
    <<: *variables
    RUN_TESTS: "OFF"
    STOCK_CPPZMQ: "OFF"
  rules:
    # This job runs only for merge requests.
    - if: $CI_MERGE_REQUEST_IID
  image: debian:10
  tags:
    - dind, skao, docker, linux, amd64
  # allow failures for next release cycle which is allowed to break ABI
  allow_failure: true
  before_script:
    - apt-get update && apt-get install -y git docker.io
    - git fetch origin "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
    - ci/before_install.sh
    - docker build -t cpp_tango ci/gcc-latest
    - >
        docker run
        --rm
        --name cpp_tango
        --user root
        -v `pwd`:/home/tango/src
        -v `pwd`/idl:/home/tango/idl
        -v `pwd`/cppzmq:/home/tango/cppzmq
        -v `pwd`/build-wrapper-linux-x86:/home/tango/build-wrapper-linux-x86
        -dit
        cpp_tango
    - ci/install_tango_idl.sh
    - if [ ${STOCK_CPPZMQ} = "OFF" ]; then ci/install_cppzmq.sh; fi
    - if [ ${STOCK_OMNIORB} = "OFF" ]; then ci/install_omniorb.sh; fi
  script:
    - >
        docker exec
        -w /home/tango/src
        -e CI_TARGET_BRANCH=${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
        -e CMAKE_BUILD_PARALLEL_LEVEL=$(nproc)
        -e GIT_COMMITTER_NAME="$GITLAB_USER_NAME"
        -e GIT_COMMITTER_EMAIL="$GITLAB_USER_EMAIL"
        cpp_tango
        ci/check-ABI-API-compliance.sh
  after_script:
    - docker stop cpp_tango
  artifacts:
    when: always
    paths:
      - compat_reports

llvm-latest:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: llvm-latest
    RUN_TESTS: "OFF"
    TANGO_WARNINGS_AS_ERRORS: "ON"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    STOCK_CPPZMQ: "OFF"

gcc-latest:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: gcc-latest
    RUN_TESTS: "OFF"
    TANGO_WARNINGS_AS_ERRORS: "ON"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    STOCK_CPPZMQ: "OFF"

ubuntu-20.04:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: ubuntu-20.04
    STOCK_CPPZMQ: "OFF"

coverage:
  <<: *job-template
  variables:
    <<: *variables
    TANGO_ENABLE_COVERAGE: "ON"
    OS_TYPE: debian10
    STOCK_CPPZMQ: "OFF"
  after_script:
    - mkdir -p build/tests/results
    - mkdir coverage
    - >
        docker exec --workdir /home/tango/src cpp_tango
        gcovr --filter '^cppapi/' --filter '^log4tango/(?!tests/)' -j$(nproc)
        --xml --output coverage.xml
    - >
        docker exec --workdir /home/tango/src cpp_tango
        gcovr --filter '^cppapi/' --filter '^log4tango/(?!tests/)' -j$(nproc)
        --html-details --output coverage/coverage.html
    - >
        docker exec --workdir /home/tango/src cpp_tango
        gcovr --filter '^cppapi/' --filter '^log4tango/(?!tests/)' -j$(nproc)
    - tar czf coverage.tar.gz coverage
    - docker stop cpp_tango
  artifacts:
    when: always
    reports:
      coverage_report:
        # coverage report provides line-by-line info
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - build/tests/results
      - build/tests/core.*
      - coverage.xml
      - coverage.tar.gz
  # keyword/regex to extract total coverage % for this CI job (which is also called "coverage")
  coverage: '/^TOTAL.*\s+(\d+\%)$/'

debian11-static:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian11
    RUN_TESTS: "OFF"
    BUILD_SHARED_LIBS: "OFF"
    STOCK_CPPZMQ: "OFF"

debian11-no-pch:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian11
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    RUN_TESTS: "OFF"
    STOCK_CPPZMQ: "OFF"

debian11-release:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian11
    CMAKE_BUILD_TYPE: Release
    STOCK_CPPZMQ: "OFF"

debian11:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian11
    STOCK_CPPZMQ: "OFF"

debian11-no-jpeg:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian11
    STOCK_CPPZMQ: "OFF"
    TANGO_USE_JPEG: "OFF"

debian11-cross-32bit:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian11-cross-32bit
    CMAKE_BUILD_TYPE: "Debug"
    RUN_TESTS: "OFF"
    STOCK_CPPZMQ: "OFF"
    TOOLCHAIN_FILE: "configure/toolchain-i686.cmake"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    TANGO_WARNINGS_AS_ERRORS: "ON"
    TANGO_USE_JPEG: "OFF"

debian10:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian10
    STOCK_CPPZMQ: "OFF"

ubuntu-20.04-jpeg9:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: ubuntu-20.04
    STOCK_CPPZMQ: "OFF"
    JPEG_LIB: libjpeg9-dev

clang-analyzer:
  image: ubuntu:focal
  tags:
    - docker, linux, amd64
  variables:
    DEBIAN_FRONTEND: noninteractive
    CC: /usr/lib/llvm-12/bin/clang
    CXX: /usr/lib/llvm-12/bin/clang++
  services: []
  before_script:
    - apt-get update
    # add kitware repository for cmake
    - apt-get install -y ca-certificates gpg wget
    - wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null
      | gpg --dearmor -
      | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
    - echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main'
      | tee /etc/apt/sources.list.d/kitware.list >/dev/null
    - apt-get update
    - >
      apt-get install -y
      clang-tools-12
      cmake
      curl
      git
      omniidl
      libcos4-dev
      libomniorb4-dev
      libomnithread4-dev
      libzmq3-dev
      libjpeg-dev
    # Install cppzmq
    - git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - cmake -B /cppzmq/build -DCPPZMQ_BUILD_TESTS=OFF /cppzmq
    - make -C /cppzmq/build install
    # Install tango-idl
    - mkdir /idl
    - git clone -b $TANGO_IDL_TAG --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - cmake -B /idl/build /idl
    - make -C /idl/build install
    # Create build directory for cppTango.
    - mkdir build
    # scan-build expects 'clang' binary to be available on PATH.
    - ln -s /usr/bin/clang-12 /usr/local/bin/clang
    - ln -s /usr/bin/clang-extdef-mapping-12 /usr/local/bin/clang-extdef-mapping
  script:
    - >
      cmake . -B build
      -Werror=dev
      -DCMAKE_BUILD_TYPE=Debug
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
      -DBUILD_TESTING=OFF
      -DCMAKE_DISABLE_PRECOMPILE_HEADERS=ON
    - make -C build idl_source
    # scan-build command must be wrapped in quotes because
    # --analyzer-config option cannot contain whitespaces.
    - "/usr/share/clang/scan-build-py-12/bin/analyze-build \
      -v \
      --cdb build/compile_commands.json \
      --exclude build/cppapi/server/idl \
      --keep-empty \
      --output clang-analyzer-results \
      --force-analyze-debug-code \
      --analyzer-config \
        stable-report-filename=true,\
        aggressive-binary-operation-simplification=true \
      --enable-checker core \
      --enable-checker cplusplus \
      --enable-checker deadcode \
      --enable-checker nullability \
      --enable-checker optin.cplusplus \
      --enable-checker optin.performance \
      --enable-checker optin.portability \
      --enable-checker security \
      --enable-checker unix \
      --enable-checker alpha.clone \
      --enable-checker alpha.core \
      --enable-checker alpha.cplusplus \
      --enable-checker alpha.deadcode \
      --enable-checker alpha.nondeterminism \
      --enable-checker alpha.unix \
      > clang-analyzer-output.txt"
  artifacts:
    when: always
    paths:
      - clang-analyzer-output.txt
      - clang-analyzer-results

clang-tidy:
  image: ubuntu:focal
  tags:
    - docker, linux, amd64
  variables:
    DEBIAN_FRONTEND: noninteractive
    CC: /usr/lib/llvm-11/bin/clang
    CXX: /usr/lib/llvm-11/bin/clang++
  services: []
  before_script:
    - apt-get update
    # add kitware repository for cmake
    - apt-get install -y ca-certificates gpg wget
    - wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null
      | gpg --dearmor -
      | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
    - echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main'
      | tee /etc/apt/sources.list.d/kitware.list >/dev/null
    - apt-get update
    - >
      apt-get install -y
      clang-tidy-11
      cmake
      git
      omniidl
      python3-minimal
      ruby-dev
      libcos4-dev
      libomniorb4-dev
      libomnithread4-dev
      libzmq3-dev
      libjpeg-dev
    # Install cppzmq
    - git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - cmake -B /cppzmq/build -DCPPZMQ_BUILD_TESTS=OFF /cppzmq
    - make -C /cppzmq/build install
    # Install tango-idl
    - git clone -b $TANGO_IDL_TAG --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - cmake -B /idl/build /idl
    - make -C /idl/build install
    # Install codeclimate. This is needed for generating HTML report.
    - git clone -b v0.85.22 --depth 1 https://github.com/codeclimate/codeclimate.git /codeclimate
    - gem build -C /codeclimate /codeclimate/codeclimate.gemspec
    - gem install /codeclimate/codeclimate-*.gem
    # Create build directory for cppTango.
    - mkdir build
  script:
    - >
      cmake . -B build
      -Werror=dev
      -DCMAKE_BUILD_TYPE=Debug
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
      -DCMAKE_UNITY_BUILD=ON
      -DCMAKE_UNITY_BUILD_BATCH_SIZE=0
      -DBUILD_TESTING=OFF
      -DCMAKE_DISABLE_PRECOMPILE_HEADERS=ON
    - make -C build idl_source
    - >
      run-clang-tidy-11
      -p build -header-filter='.*' 'cppapi/(?!server/idl)' 'log4tango/src'
      > clang-tidy-output.txt
    # Print warning summary (check name and occurrence count).
    - >
      grep -E 'warning: .+ \[.+\]$' clang-tidy-output.txt
      | sort | uniq | sed -E 's|^.+ \[(.+)\]|\1|' | sort | uniq -c
    # Produce a report in Code Climate JSON format.
    - >
      cat clang-tidy-output.txt
      | ./ci/clang-tidy-to-code-climate.py "$(pwd)/"
      > code-quality-report.json
    # Produce a report in HTML format.
    - >
      cat code-quality-report.json
      | ruby -I/codeclimate/lib ci/code-climate-json-to-html.rb "$(pwd)"
      > code-quality-report.html
  artifacts:
    when: always
    reports:
       codequality: code-quality-report.json
    paths:
      - clang-tidy-output.txt
      - code-quality-report.json
      - code-quality-report.html

# job to build against custom library locations and minimum supported versions of dependent libraries
cmake-with-locations-and-min-versions:
  image: debian:buster
  tags:
    - docker, linux, amd64
  variables:
    DEBIAN_FRONTEND: noninteractive
  services: []
  parallel:
    matrix:
      - BUILD_TYPE: [Release, Debug, RelWithDebInfo, MinSizeRel]
  before_script:
    - apt-get update
    - >
      apt-get install -y
      build-essential
      curl
      git
      pkg-config
      python3-dev
      python3-minimal
      libtool
      autoconf
      nasm
    # Install cmake
    - curl -L https://cmake.org/files/v3.18/cmake-3.18.0-Linux-x86_64.sh -o /tmp/cmake-install.sh
    - chmod +x /tmp/cmake-install.sh
    - cd /usr/local
    - /tmp/cmake-install.sh --skip-license --exclude-subdir
    # Install tango-idl
    - git clone -b $TANGO_IDL_TAG --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - mkdir /idl/build
    - cd /idl/build
    - cmake -DCMAKE_INSTALL_PREFIX=/usr/local/tango-idl /idl
    - make install
    # Install libzmq
    - git clone -b v4.0.5 --depth 1 https://github.com/zeromq/zeromq4-x.git /zmq
    - mkdir /zmq/build
    - cd /zmq/build
    - cmake -DCMAKE_INSTALL_PREFIX=/usr/local/zmq /zmq
    - make install
    # Install cppzmq
    - git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - mkdir /cppzmq/build
    - cd /cppzmq/build
    - export PKG_CONFIG_PATH=/usr/local/zmq/lib/pkgconfig
    - cmake -DCPPZMQ_BUILD_TESTS=OFF -DCMAKE_INSTALL_PREFIX=/usr/local/cppzmq /cppzmq
    - make install
    # Install omniORB
    - curl -L https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-4.2.2/omniORB-4.2.2.tar.bz2/download -o /omniORB.tar.bz2
    - mkdir /omniORB
    - tar xaf /omniORB.tar.bz2 --strip-components=1 -C /omniORB
    - cd /omniORB
    - ./configure --prefix=/usr/local/omniORB
    - make
    - make install
    # Install libjpeg
    - git clone -b 1.5.1 --depth 1 https://github.com/libjpeg-turbo/libjpeg-turbo.git /libjpeg
    - mkdir /libjpeg/build
    - cd /libjpeg
    - autoreconf -fiv
    - cd build
    - ../configure -- prefix=/usr/local/jpeg
    - make
    - make install
  script:
    # using TANGO_XXX_BASE
    - cd $CI_PROJECT_DIR
    - mkdir build_base
    - cd build_base
    - >
      cmake ..
      -Werror=dev
      -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
      -DBUILD_TESTING=ON
      -DTANGO_USE_JPEG=ON
      -DTANGO_CPPZMQ_BASE=/usr/local/cppzmq
      -DTANGO_IDL_BASE=/usr/local/tango-idl
      -DTANGO_OMNI_BASE=/usr/local/omniORB
      -DTANGO_ZMQ_BASE=/usr/local/zmq
      -DTANGO_JPEG_BASE=/usr/local/jpeg
    - make
    # using PKG_CONFIG_PATH
    - cd $CI_PROJECT_DIR
    - mkdir build_pkg_config
    - cd build_pkg_config
    - export PKG_CONFIG_PATH="/usr/local/omniORB/lib/pkgconfig:/usr/local/jpeg/lib/pkgconfig:/usr/local/tango-idl/lib/pkgconfig:/usr/local/zmq/lib/pkgconfig"
    - cmake ..
      -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
      -DBUILD_TESTING=ON
      -DTANGO_USE_JPEG=ON
      -DTANGO_CPPZMQ_BASE=/usr/local/cppzmq
    - make

alpine:
  image: alpine:3.15
  tags:
    - docker, linux, amd64
  services: []
  before_script:
    - apk update
    - >
      apk add
      g++
      cmake
      curl
      git
      python3-dev
      python3
      libzmq
      make
      zeromq-dev
      jpeg-dev
    # Install tango-idl
    - git clone -b $TANGO_IDL_TAG --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - cmake -B /idl/build -DCMAKE_INSTALL_PREFIX=/usr/local/tango-idl /idl
    - make -C /idl/build install
    # Install cppzmq
    - git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - export PKG_CONFIG_PATH=/usr/local/zmq/lib/pkgconfig
    - cmake -B /cppzmq/build -DCMAKE_INSTALL_PREFIX=/usr/local/cppzmq -DCPPZMQ_BUILD_TESTS=OFF /cppzmq
    - make -C /cppzmq/build install
    # Install omniORB
    - curl -L https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-4.2.5/omniORB-4.2.5.tar.bz2/download -o /omniORB.tar.bz2
    - mkdir /omniORB
    - tar xaf /omniORB.tar.bz2 --strip-components=1 -C /omniORB
    - cd /omniORB
    - ./configure --prefix=/usr/local/omniORB
    - make
    - make install
    - cd $CI_PROJECT_DIR
    - mkdir build
  script:
    - cd build
    - >
      cmake ..
      -Werror=dev
      -DCMAKE_BUILD_TYPE=Debug
      -DBUILD_TESTING=ON
      -DTANGO_CPPZMQ_BASE=/usr/local/cppzmq
      -DTANGO_IDL_BASE=/usr/local/tango-idl
      -DTANGO_OMNI_BASE=/usr/local/omniORB
      -DTANGO_ZMQ_BASE=/usr/local/zmq
    - make

doxygen-documentation:
  image: debian:buster
  tags:
    - docker, linux, amd64
  variables:
    DEBIAN_FRONTEND: noninteractive
  services: []
  before_script:
    - apt-get update
    - >
      apt-get install -y
      build-essential
      curl
      doxygen
      git
      graphviz
      libcos4-dev
      libomniorb4-dev
      libomnithread4-dev
      libzmq3-dev
      omniidl
      libjpeg-dev
      pkg-config
    # install cmake 3.18
    - curl -L https://cmake.org/files/v3.18/cmake-3.18.0-Linux-x86_64.sh -o /tmp/cmake-install.sh
    - chmod +x /tmp/cmake-install.sh
    - /tmp/cmake-install.sh --prefix=/usr/local --skip-license --exclude-subdir
    # Install tango-idl
    - git clone -b $TANGO_IDL_TAG --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - cmake -B /idl/build -DCMAKE_INSTALL_PREFIX=/usr/local/tango-idl /idl
    - make -C /idl/build install
    # Install cppzmq
    - git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - cmake -B /cppzmq/build -DCPPZMQ_BUILD_TESTS=OFF /cppzmq
    - make -C /cppzmq/build install
    - mkdir build
  script:
    - >
      cmake . -B build
      -Werror=dev
      -DCMAKE_BUILD_TYPE=Debug
      -DBUILD_TESTING=OFF
      -DTANGO_IDL_BASE=/usr/local/tango-idl
    - make -C build doc
  artifacts:
    when: always
    paths:
      - build/doc_html

sanitizer:
  <<: *job-template
  # Failures are temporarily allowed until sanitizer issues are fixed.
  allow_failure: true
  image: ubuntu:focal
  tags:
    - dind, skao, docker, linux, amd64
  variables:
    <<: *variables
    CC: /usr/lib/llvm-12/bin/clang
    CXX: /usr/lib/llvm-12/bin/clang++
    ASAN_OPTIONS: detect_stack_use_after_return=1
    TSAN_OPTIONS: second_deadlock_stack=1
    UBSAN_OPTIONS: "\
      print_stacktrace=1,\
      report_error_type=1,\
      halt_on_error=1,\
      suppressions=$CI_PROJECT_DIR/tests/suppressions-ubsan.supp"
  # Increase timeout, this is required by UBSAN job.
  timeout: 2h
  parallel:
    matrix:
      - SANITIZER: [ASAN, TSAN, UBSAN]
  rules:
    - if: '$CI_JOB_NAME == "sanitizer: [UBSAN]"'
      allow_failure: false
      when: on_success
    - when: on_success
  before_script:
    - ulimit -c unlimited
    - echo "core.%e.%p.%t" > /proc/sys/kernel/core_pattern
    - echo "1" > /proc/sys/net/ipv4/ip_forward
    - apt-get update
    # add kitware repository for cmake
    - apt-get install -y ca-certificates gpg wget
    - wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null
      | gpg --dearmor -
      | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
    - echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main'
      | tee /etc/apt/sources.list.d/kitware.list >/dev/null
    - apt-get update
    - >
      apt-get install -y
      clang-12
      cmake
      docker.io
      gdb
      git
      iproute2
      libcos4-dev
      libomniorb4-dev
      libomnithread4-dev
      libzmq3-dev
      omniidl
      python3-minimal
      libjpeg-dev
    # Setup a route to the containers running inside dind.
    - >
      ip route add
      $(docker network inspect -f '{{(index .IPAM.Config 0).Subnet}}' bridge)
      via
      $(getent hosts docker | awk '{ print $1 }')
      dev eth0
    # Symbolizer is required for sanitizers.
    - ln -s /usr/lib/llvm-12/bin/llvm-symbolizer /usr/local/bin
    # Install tango-idl
    - git clone -b $TANGO_IDL_TAG --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - cmake -B /idl/build /idl
    - make -C /idl/build install
    # Install cppzmq
    - git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - cmake -B /cppzmq/build -DCPPZMQ_BUILD_TESTS=OFF /cppzmq
    - make -C /cppzmq/build install
    # Create build directory for cppTango.
    - mkdir build
  script:
    - cd build
    - >
      cmake ..
      -Werror=dev
      -DCMAKE_BUILD_TYPE=Debug
      -DBUILD_TESTING=ON
      -DCMAKE_DISABLE_PRECOMPILE_HEADERS=OFF
      -DTANGO_ENABLE_SANITIZER=$SANITIZER
    - make
    - docker pull registry.gitlab.com/tango-controls/docker/mysql:5.16-mysql-5
    - docker pull registry.gitlab.com/tango-controls/docker/tango-db:5.16
    - ctest -j$(nproc) -E '(event::per_event|old_tests::ring_depth)' --output-on-failure

# Trigger conda dev package build for the default branch
build-conda-dev-package:
  stage: test
  variables:
    DEPLOY_PACKAGE: "true"
  trigger:
    project: tango-controls/conda/cpptango-feedstock
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

libcpp:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: llvm-latest
    RUN_TESTS: "OFF"
    STOCK_CPPZMQ: "OFF"
    TANGO_WARNINGS_AS_ERRORS: "ON"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    TANGO_USE_LIBCPP: "ON"

pkgconfig-fedora37:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: fedora37
    RUN_TESTS: "OFF"
    STOCK_CPPZMQ: "OFF"
    JPEG_LIB: "libjpeg-turbo-devel"
    TANGO_USE_JPEG: "OFF"
  script:
    - ci/run-pkg-config-validation.sh

cmake-superproject:
  image: debian:bullseye
  variables:
    DEBIAN_FRONTEND: noninteractive
  services: []
  tags:
    - docker, linux, amd64
  before_script:
    - apt-get update
    - >
      apt-get install -y
      build-essential
      cmake
      doxygen
      git
      graphviz
      libcos4-dev
      libomniorb4-dev
      libomnithread4-dev
      libzmq3-dev
      omniidl
      libjpeg-dev
      pkg-config
    # Install tango-idl
    - git clone --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - cmake -B /idl/build -DCMAKE_INSTALL_PREFIX=/usr/local/tango-idl /idl
    - make -C /idl/build install
    # Install cppzmq
    - git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - cmake -B /cppzmq/build -DCPPZMQ_BUILD_TESTS=OFF /cppzmq
    - make -C /cppzmq/build install
    - mkdir build
  script:
    - cd tests/superproject
    - >
      cmake . -B build
      -DCMAKE_BUILD_TYPE=Debug
      -DBUILD_TESTING=OFF
      -DTANGO_IDL_BASE=/usr/local/tango-idl
    - cmake --build build
